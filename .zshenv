# custom scripts

export PATH=~/.scripts/:$PATH

# Node Project Setup

export PATH=./node_modules/.bin:$PATH

# NeoVim Alias

alias v=nvim

