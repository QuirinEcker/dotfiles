#!/bin/bash

echo Yay bootstrap

yay -Syu

if [ $? -ne 0 ]; then
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si
    cd ..
    rm -rf yay
    yay -Syu
fi


echo Software Bootstrap

packages=(
    neovim # editor
    gnome-shell # desktop environment
    gdm # display manager
    zsh # shell
    gnome-control-center # setting
    gnome-browser-connector # installing extensions from the browser
    gnome-shell-extension-forge # tiling window manager extension
    gnome-shell-extension-blur-my-shell # fancy blur for the shell
    firefox # browser
    ttf-meslo-nerd-font-powerlevel10k # font
    keyd # remapping 
    neofetch
    discord
    python
    npm
    ripgrep
)

yay -Syu --needed --needed ${packages[@]}

echo Copy Root Configs

if [[ ! -f /etc/keyd/default.conf ]]; then
    sudo ln ~/.config/.root/etc/keyd/default.conf /etc/keyd/default.conf
fi

echo Enabling Gnome Extensions

gnome-extensions enable blur-my-shell@aunetx 
gnome-extensions enable forge@jmmaranan.com 
