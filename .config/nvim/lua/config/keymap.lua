local keymap = vim.keymap.set
keymap({'v', 'n'}, ' ', '', {})

local function vn_map(map, action)
    keymap({'v', 'n'}, map, action)
end

-- Clipboard
vn_map('<leader>y', [["+y]])
keymap('n', '<leader>yy', [["+yy]])
vn_map('<leader>op', [[o<Esc>"+p]])
vn_map('<leader>p', [["+p]])

-- saving, buffer and windows
vn_map('<leader>cl', ':bd <cr>')
vn_map('<leader>ss', ':w <cr>')
vn_map('<leader>sq', ':wq <cr>')
vn_map('<leader>sc', ':w <cr> :bd <cr>')

-- Trouble
vn_map('<leader>dl', ':Trouble <cr>')

-- jumping
-- require("mini.bracketed").setup()

