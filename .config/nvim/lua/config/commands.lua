local lib = require('lib.window')

local command = function(name, callback)
    vim.api.nvim_create_user_command(name, callback, {})
end

command('Settings', function()
    local path = vim.fn.expand('~/.config/nvim/')
    lib.new_window(path, 'Settings')
end)

command('TransparencyFix', function ()
    require('lualine').setup(require('plugins.lualine').opts)
end)

