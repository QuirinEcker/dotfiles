local dropdown_configs = {
    layout_strategy = 'horizontal',
    layout_config = {
        prompt_position = 'bottom',
        horizontal = {
            width = 0.8,
            height = 100,
        },
    },
}

local function find_files()
    require("telescope.builtin").find_files()
end

local function find_references()
    require('telescope.builtin').lsp_references()
end

return {
    'nvim-telescope/telescope.nvim',
    config = function()
        require('telescope').setup({
            extensions = {
                ['ui-select'] = {
                    require('telescope.themes').get_dropdown(dropdown_configs),
                },
            },
            pickers = {
                find_files = {
                    hidden = true,
                    find_command = { 'rg', '--files', '--hidden', '--glob', '!**/.git/*' },
                }
            },
        })

        require('telescope').load_extension('ui-select')
    end,
    keys = {
        { '<leader>ff', find_files, desc = "(f)ind (f)iles" },
        { '<leader>gr', find_references, desc = "(g)o to (r)eferences" }
    },
    cmd = "Telescope",
    dependencies = {
        'nvim-telescope/telescope-ui-select.nvim',
        'nvim-lua/plenary.nvim',
        'kyazdani42/nvim-web-devicons',
    }
}
