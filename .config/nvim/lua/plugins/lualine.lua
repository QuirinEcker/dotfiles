local function CurrentTime()
    return os.date("%H:%M:%S")
end

-- Bubbles config for lualine
-- Author: lokesh-krishna
-- MIT license, see LICENSE for more details.

-- stylua: ignore
local colors = {
    blue        = '#80a0ff',
    cyan        = '#79dac8',
    transparent = nil,
    white       = '#c6c6c6',
    red         = '#ff5189',
    violet      = '#d183e8',
    grey        = '#303030',
}

local bubbles_theme = {
    normal = {
        a = { fg = colors.transparent, bg = colors.violet },
        b = { fg = colors.white, bg = colors.grey },
        c = { fg = colors.transparent, bg = colors.transparent },
    },

    insert = { a = { fg = colors.transparent, bg = colors.blue } },
    visual = { a = { fg = colors.transparent, bg = colors.cyan } },
    replace = { a = { fg = colors.transparent, bg = colors.red } },

    inactive = {
        a = { fg = colors.white, bg = colors.transparent },
        b = { fg = colors.white, bg = colors.transparent },
        c = { fg = colors.transparent, bg = colors.transparent },
    },
}

return {
    'nvim-lualine/lualine.nvim',
    event = "VeryLazy",
    lazy = false,
    opts = {
        options = {
            theme = bubbles_theme,
            component_separators = '|',
            section_separators = { left = ' ', right = ' ' },
        },
        sections = {
            lualine_a = {
                { 'mode', separator = { left = ' ', right = '' } },
            },
            lualine_b = { 'filename', 'branch', 'diff' },
            lualine_c = {},
            lualine_x = {},
            lualine_y = { 'filetype', { 'diagnostics', always_visible = true }, 'progress' },
            lualine_z = {
                { CurrentTime, separator = { right = ' ', left = '' } },
            },
        },
        inactive_sections = {
            lualine_a = {},
            lualine_b = {},
            lualine_c = {},
            lualine_x = {},
            lualine_y = {},
            lualine_z = {},
        },
        tabline = {
            lualine_a = {},
            lualine_b = {},
            lualine_c = {},
            lualine_x = {},
            lualine_y = {},
            lualine_z = {},
        },
        extensions = {},
    },
    dependencies = {
        require("plugins.theme")
    }
}
