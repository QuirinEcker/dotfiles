return {
    'folke/trouble.nvim',
    opts = {},
    keys = {
        {'<leader>dl', ':Trouble<cr>', '(d)iagnostic (l)ist'}
    },
    cmd = 'Trouble',
    dependencies = {
        require('plugins.lsp')
    }
}
