return {
    'lervag/vimtex',
    -- event = "InsertEnter",
	config = function ()
		vim.g.vimtex_view_method = "zathura"
	end,
	lazy = false,
    enabled = true,
    dependencies = {}
}