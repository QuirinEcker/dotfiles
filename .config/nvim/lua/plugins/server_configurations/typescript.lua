
return {
	config = function ()
		local lsp = require("lspconfig")
		local vuePluginPath = vim.fn.expand("~/.local/share/pnpm/global/5/node_modules/@vue/typescript-plugin")

		return {
			root_dir = lsp.util.root_pattern("package.json"),
			single_file_support = false,
			init_options = {
				plugins = {
					{
						name = "@vue/typescript-plugin",
						location = vuePluginPath,
						languages = { "javascript", "typescript", "vue" },
					},
				},
			},
			filetypes = {
				"javascript",
				"typescript",
				"vue",
			}
		}
	end
}