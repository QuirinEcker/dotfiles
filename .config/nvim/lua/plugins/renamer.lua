local function rename()
    require('renamer').rename()
end


return {
    'filipdutescu/renamer.nvim',
    keys = {
        { '<leader>rn', rename, '(r)e(n)ame' }
    },
    opts = {},
    dependencies = {
        'nvim-lua/plenary.nvim',
        require('plugins.lsp')
    }
}
