local function keymap(client, bufnr)
	-- loading workspace diagnostics
	require('workspace-diagnostics').populate_workspace_diagnostics(client, bufnr)

	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- Mappings.
	-- See `:help vim.lsp.*` for documentation on any of the below functions
	local bufopts = { noremap = true, silent = true, buffer = bufnr }
	vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
	vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
	vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
	vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
	vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
	vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
	vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
	vim.keymap.set('n', '<space>wl', function()
		print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
	end, bufopts)
	vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
	vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
	vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)
end

local function diagnostic_icons()
	local signs = {
		Error = " ",
		Warn = " ",
		Hint = " ",
		Info = " "
	}

	for type, icon in pairs(signs) do
		local hl = "DiagnosticSign" .. type
		vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
	end
end

local function lsp_zero()
	-- adding edge cases for workspace diagnostics
	require('workspace-diagnostics').setup {
		workspace_files = function()
			local gitPath = vim.fn.systemlist("git rev-parse --show-toplevel")[1]
			local workspace_files = vim.fn.split(vim.fn.system("git ls-files " .. gitPath), "\n")
			-- this makes nuxt laod the nuxt typescript definition
			table.insert(workspace_files, '.nuxt/nuxt.d.ts')
			return workspace_files
		end
	}


	local zero = require("lsp-zero")
	zero.extend_lspconfig()
	zero.on_attach(keymap)

	require('mason').setup({})
	require('mason-lspconfig').setup({
		automatic_installation = true,
		handlers = {
			zero.default_setup
		}
	})

	local lspconfig = require("lspconfig")

	lspconfig.denols.setup(require("plugins.server_configurations.deno").config())
	lspconfig.tsserver.setup(require('plugins.server_configurations.typescript').config())
	lspconfig.jsonls.setup(require('plugins.server_configurations.json'))
	lspconfig.yamlls.setup(require("plugins.server_configurations.yaml"))
end

return {
	'VonHeikemen/lsp-zero.nvim',
	event = { "BufReadPost", "BufNewFile" },
	config = function()
		lsp_zero()
		diagnostic_icons()
	end,
	dependencies = {
		{ 'williamboman/mason.nvim' },
		{ 'williamboman/mason-lspconfig.nvim' },
		'neovim/nvim-lspconfig',
		'artemave/workspace-diagnostics.nvim',
	}
}
