return {
    'tpope/vim-fugitive',
    keys = {
        {'<leader>gg', ':Git<cr>', 'Git Status'},
        {'<leader>gc', ':Git commit<cr>', 'Git commit'}
    },
    cmd = 'Git'
}
