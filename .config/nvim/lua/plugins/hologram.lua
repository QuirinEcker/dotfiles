return {
	'edluffy/hologram.nvim',
	event = 'VeryLazy',
    enabled = false,
	opts = {
		auto_display = true
	}
}