return {
    new_window = function(o_path, o_title)
        if o_path == nil then
            o_path = vim.fn.getcwd()
        end

        if o_title == nil then
            o_title = "neovim"
        end

        return io.popen(string.format("alacritty --working-directory=%s --title=%s --command nvim .", o_path, o_title))
    end
}
